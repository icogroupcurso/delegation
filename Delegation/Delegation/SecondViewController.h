//
//  SecondViewController.h
//  Delegation
//
//  Created by Andres Torres on 8/7/14.
//  Copyright (c) 2014 Andres Torres. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Configuration.h"

@interface SecondViewController : UIViewController{
    NSString* _text;
}

-(void)setText:(NSString*)text;

@end
