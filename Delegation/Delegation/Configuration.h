//
//  Configuration.h
//  Delegation
//
//  Created by Andres Torres on 8/7/14.
//  Copyright (c) 2014 Andres Torres. All rights reserved.
//

#ifndef Delegation_Configuration_h
#define Delegation_Configuration_h

//Constantes para determinar que valor de tag del tag corresponde a que color
#define RED 0
#define GREEN 1
#define BLUE 2


#endif
