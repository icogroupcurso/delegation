//
//  SecondViewController.m
//  Delegation
//
//  Created by Andres Torres on 8/7/14.
//  Copyright (c) 2014 Andres Torres. All rights reserved.
//

#import "SecondViewController.h"

@interface SecondViewController (){

    
    __weak IBOutlet UILabel *secondViewControllerLabel;

}

- (IBAction)returnToFirstViewController:(id)sender;
@end

@implementation SecondViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //Asignarle al label el text que se esta enviando desde el primer view controller
    [secondViewControllerLabel setText:_text];
}

//Este metodo se llama para regresar al primer ViewController
- (IBAction)returnToFirstViewController:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

//Esto aun no lo hemos usado hasta este punto
-(IBAction)changeColor:(UIButton*)sender{
    UIColor* color;
    switch (sender.tag) {
        case RED:
            color = [UIColor redColor];
            break;
        case GREEN:
            color = [UIColor greenColor];
            break;
        case BLUE:
            color = [UIColor blueColor];
            break;
    }
    
    [self.view setBackgroundColor:color];
    
}


//Metodo que permite asignarle un texto a la variable text
-(void) setText:(NSString*) text{
 
    _text = text;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


@end
