//
//  ViewController.m
//  Delegation
//
//  Created by Andres Torres on 8/7/14.
//  Copyright (c) 2014 Andres Torres. All rights reserved.
//

#import "ViewController.h"
#import "SecondViewController.h"

@interface ViewController (){
    
    __weak IBOutlet UITextField *textFieldToPassText;
    
}

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}




-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [textFieldToPassText resignFirstResponder];
}

//Este metodo se llama antes de cambiar de un ViewController a otro
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    //Crea una instancia del segundo viewController
    SecondViewController* sVC = segue.destinationViewController;
    //Agarra el texto del textField y lo pasa al segundo view controller
    [sVC setText:textFieldToPassText.text];
}




- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
